// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LostFrogGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class LOSTFROG_API ALostFrogGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
