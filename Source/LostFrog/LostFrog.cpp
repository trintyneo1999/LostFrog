// Copyright Epic Games, Inc. All Rights Reserved.

#include "LostFrog.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, LostFrog, "LostFrog" );
